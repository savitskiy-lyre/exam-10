DROP
    database if exists Savitskiy_Maksim_db;
CREATE
    database if not exists Savitskiy_Maksim_db;

USE
    Savitskiy_Maksim_db;

CREATE TABLE news
(
    id            int auto_increment not null PRIMARY KEY,
    title         varchar(255)       not null,
    description   text               not null,
    image         varchar(255)       null,
    creating_time TIMESTAMP DEFAULT CURRENT_TIMESTAMP
);
CREATE TABLE comments
(
    id      int auto_increment               not null PRIMARY KEY,
    id_news int                              not null,
    author  varchar(255) default 'Anonymous' null,
    comment text                             not null,
    constraint comments_news_id_fk
        foreign key (id_news)
            references news (id)
            on update cascade
            on delete cascade
);

INSERT INTO news(title, description, image)
VALUES ('Good news', 'Something good has happened', null),
       ('Bad news', 'Something bad has happened', null),
       ('Normal news', 'Something normal has happened', null);
INSERT INTO comments(id_news, author, comment)
VALUES (3, 'Ivan', 'lorem20'),
       (2, 'Olya', 'lorem30');

INSERT INTO comments(id_news, comment)
VALUES (2, 'lorem10');
