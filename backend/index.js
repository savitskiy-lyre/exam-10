const express = require("express");
const cors = require('cors');
const mysql = require('./db/mysqlDb');
const news = require('./routers/news');
const comments = require('./routers/comments');
const {PATH} = require('./constants');

const server = express();
server.use(express.json());
server.use(cors());
server.use(express.static('public'));

const port = 8080;

server.get('/', (req, res) => {
   res.send('Hello');
})
server.use(PATH.news, news);
server.use(PATH.comments, comments);

mysql.connect().catch(e => console.log(e));

server.listen(port, () => {
   console.log('Server is started on port ' + port);
})