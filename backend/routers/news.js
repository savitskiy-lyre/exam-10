const express = require('express');
const multer = require('multer');
const path = require('path');
const {nanoid} = require('nanoid');
const config = require('../config');
const mysqlDb = require('../db/mysqlDb');

const storage = multer.diskStorage({
   destination: (req, file, cb) => {
      cb(null, config.uploadPath);
   },
   filename: (req, file, cb) => {
      cb(null, nanoid() + path.extname(file.originalname));
   }
});

const upload = multer({storage});

const router = express.Router();

router.get('/', async (req, res) => {
   try {
      const [products] = await mysqlDb.getConnection().query('SELECT id, title, image, creating_time FROM news');
      res.send(products);
   } catch (err) {
      res.status(503).send(err.message);
   }
});

router.get('/:id', async (req, res) => {
   try {
      const [info] = await mysqlDb.getConnection().query(`SELECT * FROM news WHERE id = ?`, [req.params.id]);
      res.send(info[0]);
   } catch (err) {
      res.status(503).send(err.message);
   }
});

router.post('/', upload.single('image'), async (req, res) => {
   console.log(req.body)
   if (!req.body.title || !req.body.description) {
      return res.status(400).send({error: 'Data not valid'});
   }
   try {
      const news = {
         title: req.body.title,
         description: req.body.description,
      };
      if (req.file) news.image = req.file.filename;

      const [transient] = await mysqlDb.getConnection().query(
        'INSERT INTO news (title, description, image) values (?, ?, ?)',
        [news.title, news.description, news.image]
      );
      res.send({
         ...news,
         id: transient.insertId
      });
   } catch (err) {
      res.status(503).send(err.message);
   }
});

router.delete('/:id', async (req, res) => {
   try {
      const [transient] = await mysqlDb.getConnection().query(`DELETE FROM news WHERE id=?`, [req.params.id]);
      if (transient.affectedRows === 0) return res.status(404).send({error: "Not Found"});
      res.send({message: 'Deletion done'});
   } catch (err) {
      res.status(503).send(err.message);
   }
});


module.exports = router;
