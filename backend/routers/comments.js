const express = require('express');
const mysqlDb = require('../db/mysqlDb');

const router = express.Router();

router.get('/', async (req, res) => {
   try {
      let sqlString = [];
      if (req.query.id_news) {
         sqlString[0] = `SELECT * FROM comments WHERE id_news=?`;
         sqlString[1] = [req.query.id_news];
      } else {
         sqlString[0] = 'SELECT * FROM comments';
      }
      const [products] = await mysqlDb.getConnection().query(...sqlString);

      res.send(products);
   } catch (err) {
      res.status(503).send(err.message);
   }
});

router.post('/', async (req, res) => {
   if (!req.body.id_news || !req.body.comment) {
      return res.status(400).send({error: 'Data not valid'});
   }
   try {
      const comment = {
         id_news: req.body.id_news,
         comment: req.body.comment,
      };
      if (req.body.author) comment.author = req.body.author;

      if (req.body.author) {
         const [transient] = await mysqlDb.getConnection().query(
           'INSERT INTO comments ( comment, id_news, author) values (?, ?, ?)',
           [comment.comment, comment.id_news, comment.author]
         );
         res.send({
            ...comment,
            id: transient.insertId
         });
      } else {
         const [transient] = await mysqlDb.getConnection().query(
           'INSERT INTO comments (comment, id_news) values (?, ?)',
           [comment.comment, comment.id_news]
         );
         res.send({
            ...comment,
            id: transient.insertId
         });
      }
   } catch (err) {
      res.status(503).send(err.message);
   }
});

router.delete('/:id', async (req, res) => {
   try {
      const [transient] = await mysqlDb.getConnection().query(`DELETE FROM comments WHERE id=?`, [req.params.id]);
      if (transient.affectedRows === 0) return res.status(404).send({error: "Not Found"});
      res.send({message: 'Deletion done'});
   } catch (err) {
      res.status(503).send(err.message);
   }
});

module.exports = router;
