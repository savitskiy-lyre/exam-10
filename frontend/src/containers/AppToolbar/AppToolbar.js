import React from 'react';
import {AppBar, Button, Toolbar} from "@mui/material";
import {Link} from "react-router-dom";

const AppToolbar = () => {
   return (
     <>
        <AppBar position={"fixed"}>
           <Toolbar>
              <Button component={Link}
                      to='/news'
                      sx={{color: "white"}}
              >
                 News
              </Button>
           </Toolbar>
        </AppBar>
        <Toolbar/>
     </>
   );
};

export default AppToolbar;