import React, {useState} from 'react';
import {Button, Stack, TextField, Typography} from "@mui/material";
import {useDispatch} from "react-redux";
import { useHistory } from 'react-router-dom';
import {createNews} from "../../store/actions/newsActions";

const CreateNews = () => {
   const dispatch = useDispatch();
   const history = useHistory();
   const [titleInp, setTitleInp] = useState('');
   const [descriptionInp, setDescriptionInp] = useState('');
   const [imageInp, setImageInp] = useState('');

   const onSubmit = (e) => {
      e.preventDefault();
      const newNews = {
         description: descriptionInp,
      }
      if (imageInp) newNews.image = imageInp;
      if (titleInp) newNews.title = titleInp;
      const formData = new FormData();
      console.log(newNews)
      Object.keys(newNews).forEach(key => {
         formData.append(key, newNews[key]);
      });
      console.log(formData)
      dispatch(createNews(formData))
      setTitleInp('')
      setDescriptionInp('')
      history.push('/');
   };

   return (
     <>
        <Stack component={'form'} direction={"column"} spacing={3} mt={2} onSubmit={onSubmit}>
           <Typography variant={'h3'}>
              Create news
           </Typography>
           <TextField
             label="Title"
             value={titleInp}
             onChange={(e) => setTitleInp(e.target.value)}
           />
           <TextField
             required
             label="Content"
             multiline
             maxRows={4}
             value={descriptionInp}
             onChange={(e) => setDescriptionInp(e.target.value)}
           />
           <input
             type={'file'}
             onChange={(e) => setImageInp(e.target.files[0])}
           />
           <Button type={"submit"} variant="contained" color="success">Confirm</Button>
        </Stack>
     </>
   );
};

export default CreateNews;