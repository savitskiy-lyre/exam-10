import React from 'react';
import {Button, CardMedia, Grid, Typography} from "@mui/material";
import {makeStyles} from "@mui/styles";
import {apiURL} from "../../config";
import imageNotAvailable from '../../assets/images/notFoundjpg.jpg';
import TitleWIthBtn from "../../components/TitleWIthBtn/TitleWIthBtn";
import {Link} from "react-router-dom";
import {useDispatch, useSelector} from "react-redux";
import {deleteNews} from "../../store/actions/newsActions";

const useStyles = makeStyles({
   post: {
      border: '3px solid gainsboro',
      borderRadius: '4px',
      background: 'white'
   },
});

const Posts = () => {
   const dispatch = useDispatch;
   const posts = useSelector(state => state.news.data);
   const classes = useStyles();

   return posts && (
     <>
        <TitleWIthBtn title={'Posts'}/>
        <Grid container>
           {posts.map((post) => {
              return (
                <Grid item container key={post.id + post.title} className={classes.post} mt={2}
                      alignItems={"center"}
                      flexWrap={"nowrap"}>
                   <Grid item>
                      <Grid item>
                         <CardMedia
                           component="img"
                           height="120"
                           image={post.image ? apiURL + '/uploads/' + post.image : imageNotAvailable}
                           alt={post.title}
                           sx={{width: '150px'}}
                         />
                      </Grid>
                   </Grid>
                   <Grid container item height={"100%"} alignItems={"end"} px={1}>
                      <Grid item>
                         <Typography variant={'h4'}>
                            {post.title}
                         </Typography>
                      </Grid>
                      <Grid item container flexGrow={1} justifyContent={"space-between"}>
                         <Grid item>
                            <Typography variant={'body1'}>
                               {post.creating_time}
                            </Typography>
                         </Grid>
                         <Grid item mr={2}>
                            <Button color="success"
                                    component={Link}
                                    to={'/news/' + post.id}
                            >
                               Read full post
                            </Button>
                            <Button color="secondary" size="large"
                                    onClick={() => dispatch(deleteNews(post.id))}
                            >
                               delete
                            </Button>
                         </Grid>
                      </Grid>
                   </Grid>
                </Grid>
              );
           })}
        </Grid>
     </>
   );
};

export default Posts;
/*
*/
