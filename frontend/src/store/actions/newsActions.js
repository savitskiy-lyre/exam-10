import {axiosApi} from "../../axiosApi";
import {NEWS_URL} from "../../config";

export const FETCH_NEWS_REQUEST = 'FETCH_NEWS_REQUEST';
export const FETCH_NEWS_SUCCESS = 'FETCH_NEWS_SUCCESS';
export const FETCH_NEWS_FAILURE = 'FETCH_NEWS_FAILURE';
export const CREATE_NEWS_REQUEST = 'CREATE_NEWS_REQUEST';
export const CREATE_NEWS_SUCCESS = 'CREATE_NEWS_SUCCESS';
export const CREATE_NEWS_FAILURE = 'CREATE_NEWS_FAILURE';
export const DELETE_NEWS_REQUEST = 'DELETE_NEWS_REQUEST';
export const DELETE_NEWS_SUCCESS = 'DELETE_NEWS_SUCCESS';
export const DELETE_NEWS_FAILURE = 'DELETE_NEWS_FAILURE';

export const fetchNewsRequest = () => ({type: FETCH_NEWS_REQUEST});
export const fetchNewsSuccess = (data) => ({type: FETCH_NEWS_SUCCESS, payload: data});
export const fetchNewsFailure = (error) => ({type: FETCH_NEWS_FAILURE, payload: error});

export const fetchNews = () => {
   return async (dispatch) => {
      try {
         dispatch(fetchNewsRequest());
         const {data} = await axiosApi.get(NEWS_URL);
         dispatch(fetchNewsSuccess(data));
      } catch (error) {
         dispatch(fetchNewsFailure(error));
      }
   }
}
export const createNewsRequest = () => ({type: CREATE_NEWS_REQUEST});
export const createNewsSuccess = () => ({type: CREATE_NEWS_SUCCESS});
export const createNewsFailure = (error) => ({type: CREATE_NEWS_FAILURE, payload: error});

export const createNews = ({newNews}) => {
   return async (dispatch) => {
      try {
         dispatch(createNewsRequest());
         const {data} = await axiosApi.post(NEWS_URL, newNews);
         dispatch(fetchNews());
         console.log(data)
         dispatch(createNewsSuccess());
      } catch (error) {
         dispatch(createNewsFailure(error));
      }
   }
}
export const deleteNewsRequest = () => ({type: DELETE_NEWS_REQUEST});
export const deleteNewsSuccess = () => ({type: DELETE_NEWS_SUCCESS});
export const deleteNewsFailure = (error) => ({type: DELETE_NEWS_FAILURE, payload: error});

export const deleteNews = ({id}) => {
   return async (dispatch) => {
      try {
         dispatch(deleteNewsRequest());
         const {data} = await axiosApi.delete(NEWS_URL + '/' + id);
         dispatch(fetchNews());
         console.log(data)
         dispatch(deleteNewsSuccess());
      } catch (error) {
         dispatch(deleteNewsFailure(error));
      }
   }
}