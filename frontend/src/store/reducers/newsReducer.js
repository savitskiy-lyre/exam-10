import {
   CREATE_NEWS_FAILURE,
   CREATE_NEWS_REQUEST,
   CREATE_NEWS_SUCCESS, DELETE_NEWS_FAILURE, DELETE_NEWS_REQUEST, DELETE_NEWS_SUCCESS,
   FETCH_NEWS_FAILURE,
   FETCH_NEWS_REQUEST,
   FETCH_NEWS_SUCCESS
} from "../actions/newsActions";


const initState = {
   error: null,
   data: null,
};
export const newsReducer = (state = initState, action) => {
   switch (action.type) {
      case FETCH_NEWS_REQUEST:
         return {...state, error: null};
      case FETCH_NEWS_SUCCESS:
         return {...state, data: action.payload};
      case FETCH_NEWS_FAILURE:
         return {...state, error: action.payload};
      case CREATE_NEWS_REQUEST:
         return {...state, error: null};
      case CREATE_NEWS_SUCCESS:
         return {...state};
      case CREATE_NEWS_FAILURE:
         return {...state, error: action.payload};
      case DELETE_NEWS_REQUEST:
         return {...state, error: null};
      case DELETE_NEWS_SUCCESS:
         return {...state};
      case DELETE_NEWS_FAILURE:
         return {...state, error: action.payload};
      default:
         return state;
   }
}