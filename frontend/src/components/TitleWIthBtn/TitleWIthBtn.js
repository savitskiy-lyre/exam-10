import React from 'react';
import {Button, Grid, Typography} from "@mui/material";
import {Link} from "react-router-dom";

const TitleWIthBtn = ({title}) => {
   return (
        <Grid container spacing={1} alignItems={"center"} justifyContent={"space-between"} my={1}>
           <Grid item>
              <Typography variant={'h4'}>
                 {title}
              </Typography>
           </Grid>
           <Grid item>
              <Button variant="contained" color="success"
                      component={Link}
                      to={'/news/add'}
              >
                 Add new post
              </Button>
           </Grid>
        </Grid>
   );
};

export default TitleWIthBtn;