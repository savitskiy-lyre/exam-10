import Layout from "./components/UI/Layout/Layout";
import Posts from "./containers/Posts/Posts";
import {Switch, Route} from "react-router-dom";
import CreateNews from "./containers/CreateNews/CreateNews";
import {useEffect} from "react";
import {useDispatch} from "react-redux";
import {fetchNews} from "./store/actions/newsActions";


const App = () => {
   const dispatch = useDispatch();
   useEffect(() => {
      dispatch(fetchNews());
   }, [dispatch]);
   return (
     <Layout>
        <Switch>
           <Route path="/" exact render={Posts}/>
           <Route path="/news/add" exact render={CreateNews}/>
           <Route path="/news/:id" render={()=>''}/>
           <Route path="/news" render={Posts}/>
        </Switch>
     </Layout>
   );
};
export default App;
