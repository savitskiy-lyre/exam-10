import React from 'react';
import ReactDOM from 'react-dom';
import {BrowserRouter} from "react-router-dom";
import {Provider} from "react-redux";
import {applyMiddleware, combineReducers, compose, createStore} from "redux";
import thunk from "redux-thunk";
import {newsReducer} from "./store/reducers/newsReducer";
import {commentsReducer} from "./store/reducers/commentsReducer";
import App from './App';

const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;

const rootReducer = combineReducers({
   news: newsReducer,
   comments: commentsReducer,
});

const store = createStore(rootReducer, composeEnhancers(
  applyMiddleware(thunk)
));

ReactDOM.render(
  <BrowserRouter>
     <Provider store={store}>
        <App/>
     </Provider>
  </BrowserRouter>,
  document.getElementById('root')
);